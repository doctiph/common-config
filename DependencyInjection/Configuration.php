<?php

namespace Doctipharma\CommonConfigBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * Doctipharma Common Configuration
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('doctipharma_common_config');

        $this->addMailConfig($rootNode);
        $this->addCacheConfig($rootNode);

        return $treeBuilder;
    }


    private function addMailConfig(ArrayNodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->arrayNode('mail_config')
                    ->defaultValue([])
                    ->requiresAtLeastOneElement()
                    ->useAttributeAsKey('mail_config')
                    ->prototype('array')
                    ->children()
                        ->scalarNode('site_name')->end()
                    ->end()
                ->end() // mail_config
            ->end()
        ;
    }

    private function addCacheConfig(ArrayNodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->arrayNode('lsw_memcache')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('client')->isRequired()->end()
                            ->scalarNode('prefix')->defaultNull()->end()
                            ->scalarNode('alias')->defaultNull()->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
