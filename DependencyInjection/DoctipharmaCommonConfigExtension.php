<?php

namespace Doctipharma\CommonConfigBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Definition;

/**
 * Doctipharma Common Configuration
 */
class DoctipharmaCommonConfigExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $container->setParameter('doctipharma_common_config', $config);

        $this->createLswMemcache($container, $config['lsw_memcache']);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    public function createLswMemcache(ContainerBuilder $container, $caches)
    {
        foreach ($caches as $name => $cache) {
            $serviceId  = 'doctipharma_eshop.lsw_memcache.doctrine.' . $name;
            $client = new Reference(sprintf('memcache.%s', $cache['client']));

            $def = new Definition($container->getParameter('memcache.doctrine_cache.class'));
            $def->setScope(ContainerInterface::SCOPE_CONTAINER);
            $def->addMethodCall('setMemcached', [$client]);
            if ($cache['prefix']) {
                $def->addMethodCall('setPrefix', [$cache['prefix']]);
            }
            $container->setDefinition($serviceId, $def);
            if ($cache['alias']) {
                $container->setAlias($cache['alias'], $serviceId);
            }
        }
    }
}
