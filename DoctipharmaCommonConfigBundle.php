<?php
namespace Doctipharma\CommonConfigBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Common Config bundle
 */
class DoctipharmaCommonConfigBundle extends Bundle
{
}
