<?php

namespace Doctipharma\CommonConfigBundle\Manager;

/**
* Doctipharma Common Configuration
* Email manager
*/
class EmailManager
{
    /**
    * @var \Twig_Environment
    */
    private $twig;

    /**
     * Values in mail_config.yml
     * @var array
     */
    private $mailConfig;

    /**
     * @param \Twig_Environment
     * @param array doctipharma_common_config with mail_config key
     */
    public function __construct(\Twig_Environment $twig, array $commonConfig)
    {
        $this->twig       = $twig;
        $this->mailConfig = $commonConfig['mail_config'];
    }

    /**
     * Return data for smartfocus mail template
     *
     * @param string $siteId
     * @return array data for smartfocus template by site id according to mail_config.yml and template views/Mail/SITE_ID
     */
    public function getMailData($siteId)
    {
        try {
            $data = [
                'header_text' => $this->twig->render(sprintf('DoctipharmaCommonConfigBundle:Mail:%s/headerText.html.twig', $siteId)),
                'footer_text' => $this->twig->render(sprintf('DoctipharmaCommonConfigBundle:Mail:%s/footerText.html.twig', $siteId)),
                'header_html' => $this->twig->render(sprintf('DoctipharmaCommonConfigBundle:Mail:%s/headerHtml.html.twig', $siteId)),
                'footer_html' => $this->twig->render(sprintf('DoctipharmaCommonConfigBundle:Mail:%s/footerHtml.html.twig', $siteId)),
            ];
        } catch (\Twig_Error_Loader $e) {
            $data = [
                'header_text' => $this->twig->render(sprintf('DoctipharmaCommonConfigBundle:Mail:%s/headerText.html.twig', 'default')),
                'footer_text' => $this->twig->render(sprintf('DoctipharmaCommonConfigBundle:Mail:%s/footerText.html.twig', 'default')),
                'header_html' => $this->twig->render(sprintf('DoctipharmaCommonConfigBundle:Mail:%s/headerHtml.html.twig', 'default')),
                'footer_html' => $this->twig->render(sprintf('DoctipharmaCommonConfigBundle:Mail:%s/footerHtml.html.twig', 'default')),
            ];
        }

        // site_key use in template for some coditions
        $data['site_key']  = $siteId;

        // Data from config file if site id exists in mail config
        $siteId = array_key_exists($siteId, $this->mailConfig) ? $siteId : 'default';

        $data['site_name'] = $this->mailConfig[$siteId]['site_name'];

        return $data;
    }
}
