<?php

namespace Doctipharma\CommonConfigBundle\Cache\Traits;

/**
 * DrugstoreKeyGeneratorTrait
 */
trait DrugstoreKeyGeneratorTrait
{
    public static $idTypeBo          = 'bo_id';
    public static $idTypeMirakl      = 'mirakl_id';
    public static $listContext       = ['list'];
    public static $noShippingContext = ['no_shipping'];
    public static $shopContext       = ['shop'];
    public static $newsContext       = ['shop', 'news'];

    /**
     * @var int
     */
    private $shopCacheid;

    /**
     * @var string
     */
    private $shopCacheIdType;

    /**
     * @var array
     */
    private $shopCachecontext;

    /**
     * @var bool
     */
    private $shopCachegenerateCacheKey = false;

    private function initShopCacheKeyGenerator($context = [], $id = null, $isBoId = true)
    {
        $this->shopCacheid               = $id;
        $this->shopCachecontext          = $context;
        $this->shopCacheIdType           = $isBoId ? self::$idTypeBo : self::$idTypeMirakl;
        $this->shopCachegenerateCacheKey = true;
    }

    public function resetShopCacheKeyGenerator()
    {
        $this->shopCacheid = $this->shopCacheIdType = $this->shopCachecontext = null;
        $this->shopCachegenerateCacheKey = false;
    }

    public function getShopCacheKey()
    {
        $key = null;
        if ($this->shopCachegenerateCacheKey && !empty($this->shopCachecontext)) {
            $key = $this->buildShopCacheKey($this->shopCachecontext, $this->shopCacheid, $this->shopCacheIdType);
        }

        return $key;
    }

    public function getAllShopCacheKeys($id)
    {
        return [
            $this->buildShopCacheKey(self::$listContext),
            $this->buildShopCacheKey(self::$noShippingContext, $id, self::$idTypeBo),
            $this->buildShopCacheKey(self::$shopContext, $id, self::$idTypeBo),
            $this->buildShopCacheKey(self::$newsContext, $id, self::$idTypeBo),
        ];
    }

    public function getShopCacheTags($context = [], $boId = null)
    {
        $tags = ['shop'];
        if (isset($boId)) {
            $tags[] = $this->buildShopCacheTag([], $boId);
            if (!empty($context)) {
                $tags[] = $this->buildShopCacheTag($context, $boId);
            }
        } else {
            $tags[] = $this->buildShopCacheTag($context);
        }

        return $tags;
    }


    private function buildShopCacheKey($context = [], $id = null, $idType = null)
    {
        asort($context);
        if (isset($id) && isset($idType)) {
            $key = sprintf('shop[%s:%s][%s]', $idType, $id, join('|', $context));
        } else {
            $key = sprintf('shop[%s]', join('|', $context));
        }
        return $key;
    }

    private function buildShopCacheTag($context = [], $boId = null)
    {
        $tag = "";
        if (empty($context)) {
            $tag = sprintf('shop:%s', $boId);
        } else {
            asort($context);
            if (isset($boId)) {
                $tag = sprintf('shop:%s:%s', $boId, join('_', $context));
            } else {
                $tag = sprintf('shop:%s', join('_', $context));
            }
        }
        return $tag;
    }
}
