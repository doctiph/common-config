# DoctipharmaCommonConfigBundle

Available configuration:

- [**Experian Email**](#markdown-header-experian-email), inject header, footer and some vars for Experian.

### Install

1. add composer reposirories

```json
    "repositories": [
        {
            "type": "git",
            "url": "https://doctiph@bitbucket.org/doctiph/common-config.git"
        }
    ]
```

2. Composer require

```sh
composer require doctipharma/commonconfig:dev-master --prefer-dist
```


3. AppKernel

```php
new Doctipharma\CommonConfigBundle\DoctipharmaCommonConfigBundle(),
```


4. Config.yml

```yaml
imports:
    - { resource: @DoctipharmaCommonConfigBundle/Resources/config/common_config.yml }
```


### Experian Email

#### Usage

- Add **site_key** in smarfocus context with site id value (doctipharma, default, ...)  

#### Customize header/footer of Experian mails by site id

Experian meta vars :

    - header_text
    - footer_text
    - header_html
    - footer_html
    - site_name (display name)
    - site_key (siteId)

Directory structure: 
```
DoctipharmaCommonConfigBundle
├── Resources/
│   ├── views
│   │   ├── Mail
│   │   │   ├── SITE_ID
│   │   │   |   ├── footerHtml.html.twig
│   │   │   |   ├── footerText.html.twig
│   │   │   |   ├── headerHtml.html.twig
│   │   │   |   ├── headerText.html.twig
```



Doctipharma/CommonConfigBundle/Resources/config/mail_config.yml

```yaml
doctipharma_common_config:
    mail_config:
        default:
            site_name: "notre site"
        doctipharma:
            site_name: "Doctipharma"
```
