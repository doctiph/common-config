<?php

namespace Doctipharma\CommonConfigBundle\Mailer;

use La\MessagingBundle\Adapter\SmartFocusAdapter as BaseAdapteur;
use La\Lib\Messaging\Mailer\Mailer;
use La\MessagingBundle\Spool\Spool;
use Doctipharma\CommonConfigBundle\Manager\EmailManager;

/**
 * Doctipharma SmartFocusAdapter
 */
class SmartFocusAdapter extends BaseAdapteur
{
    /**
     * @var EmailManager
     */
    private $emailManager;

    /**
     * @param Mailer $mailer
     * @param array $spool
     * @param EmailManager $emailManager
     */
    public function __construct(Mailer $mailer, array $spool, EmailManager $emailManager)
    {
        parent::__construct($mailer, $spool);
        $this->emailManager = $emailManager;
    }

    /**
     * {@inheritdoc}
     */
    public function createAndSend($to = array(), $context = array(), array $parameters = array(), &$failures)
    {
        if (array_key_exists('SITE_KEY', $context) && $context['SITE_KEY'] !== null) {
            $context = array_merge($context, $this->emailManager->getMailData($context['SITE_KEY']));
        }

        return parent::createAndSend($to, $context, $parameters, $failures);
    }
}
